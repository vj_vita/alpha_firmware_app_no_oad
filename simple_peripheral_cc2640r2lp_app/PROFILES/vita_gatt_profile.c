/******************************************************************************

 @file       vita_gatt_profile.c

 @brief This file contains the Vita GATT profile sample GATT service profile
        for use with the BLE sample application.

 Group: CMCU, SCS
 Target Device: CC2640R2

 ******************************************************************************
 
 Copyright (c) 2010-2017, Texas Instruments Incorporated
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

 *  Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

 *  Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

 *  Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 ******************************************************************************
 Release Name: simplelink_cc2640r2_sdk_01_50_00_58
 Release Date: 2017-10-17 18:09:51
 *****************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include <icall.h>
#include "util.h"
/* This Header file contains all BLE API and icall structure definition */
#include "icall_ble_api.h"

#include "vita_gatt_profile.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        29

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
// Vita GATT Profile Service UUID: 0xAA70
CONST uint8 VitaProfileServUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITAPROFILE_SERV_UUID), HI_UINT16(VITAPROFILE_SERV_UUID)
};
#ifdef VITA_BIA_SCAN
// Characteristic 1 UUID: 0xAA71
CONST uint8 VitaProfileDataUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_DATA_UUID), HI_UINT16(VITA_DATA_UUID)
};

// Characteristic 2 UUID: 0xAA72
CONST uint8 VitaProfileConfUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_CONF_UUID), HI_UINT16(VITA_CONF_UUID)
};
#endif
#ifdef BLE_DEBUG
// Characteristic 3 UUID: 0xAA73
CONST uint8 VitaProfileDebugUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_DEBUG_UUID), HI_UINT16(VITA_DEBUG_UUID)
};

// Characteristic 4 UUID: 0xAA74
CONST uint8 VitaProfileDebugConfUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_DEBUG_CONF_UUID), HI_UINT16(VITA_DEBUG_CONF_UUID)
};
#endif
// Characteristic 5 UUID: 0xAA75
CONST uint8 VitaProfileSpiReqUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_SPI_REQ_UUID), HI_UINT16(VITA_SPI_REQ_UUID)
};

// Characteristic 6 UUID: 0xAA76
CONST uint8 VitaProfileSpiRespUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_SPI_RESP_UUID), HI_UINT16(VITA_SPI_RESP_UUID)
};
#ifdef VITA_NIR_SCAN
// Characteristic 7 UUID: 0xAA77
CONST uint8 VitaProfileNirConfUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_NIR_CONF_UUID), HI_UINT16(VITA_NIR_CONF_UUID)
};

// Characteristic 8 UUID: 0xAA78
CONST uint8 VitaProfileNirDataUUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(VITA_NIR_DATA_UUID), HI_UINT16(VITA_NIR_DATA_UUID)
};
#endif
/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static VitaProfileCBs_t *VitaProfile_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// Vita Profile Service attribute
static CONST gattAttrType_t VitaProfileService = { ATT_BT_UUID_SIZE, VitaProfileServUUID };
#ifdef VITA_BIA_SCAN
// 1. Characteristic Value: data
static uint8_t VitaProfileData[VITA_DATA_LEN] = {0};

// 1. Characteristic Properties: data
static uint8_t VitaProfileDataProps = GATT_PROP_READ | GATT_PROP_NOTIFY;

// 1. Characteristic Description: data
static uint8 VitaProfileDataUserDesp[10] = "BIA Data";

// 1. Characteristic Configuration
static gattCharCfg_t *VitaProfileDataConfig;

// 2. Characteristic Properties: configuration
static uint8_t VitaProfileConfProps = GATT_PROP_READ | GATT_PROP_WRITE;

// 2. Characteristic Value: configuration
static uint8_t VitaProfileConf[VITA_CONF_LEN] = {0};

// 2. Characteristic User Description: configuration
static uint8_t VitaProfileConfUserDesp[10] = "BIA Conf";
#endif
#ifdef BLE_DEBUG
// 3. Characteristic Value: debug
static uint8_t VitaProfileDebug[VITA_DEBUG_LEN] = {0};

// 3. Characteristic Properties: debug
static uint8_t VitaProfileDebugProps = GATT_PROP_READ | GATT_PROP_NOTIFY;

// 3. Characteristic Description: data
static uint8 VitaProfileDebugUserDesp[10] = "Debug Data";

// 3. Characteristic Configuration
static gattCharCfg_t *VitaProfileDebugConfig;

// 4. Characteristic Properties: debug configuration
static uint8_t VitaProfileDebugConfProps = GATT_PROP_READ | GATT_PROP_WRITE;

// 4. Characteristic Value: debug configuration
static uint8_t VitaProfileDebugConf[VITA_DEBUG_CONF_LEN] = {0};

// 4. Characteristic User Description: configuration
static uint8_t VitaProfileDebugConfUserDesp[10] = "Debug Conf";
#endif
// 5. Characteristic Value: SPI Response
static uint8_t VitaProfileSpiResp[VITA_SPI_RESP_LEN] = {0};

// 5. Characteristic Properties: SPI Response
static uint8_t VitaProfileSpiRespProps = GATT_PROP_READ | GATT_PROP_NOTIFY;

// 5. Characteristic User Description: SPI Response
static uint8_t VitaProfileSpiRespUserDesp[10] = "SPI Resp";

// 5. Characteristic Configuration
static gattCharCfg_t *VitaProfileSpiRespConfig;

// 6. Characteristic Value: SPI Request
static uint8_t VitaProfileSpiReq[VITA_SPI_REQ_LEN] = {0};

// 6. Characteristic Properties: SPI Request
static uint8_t VitaProfileSpiReqProps = GATT_PROP_READ | GATT_PROP_WRITE;

// 6. Characteristic User Description: SPI Request
static uint8_t VitaProfileSpiReqUserDesp[10] = "SPI Req";
#ifdef VITA_NIR_SCAN
// 7. Characteristic Value: NIR data
static uint8_t VitaProfileNirData[VITA_NIR_DATA_LEN] = {0};

// 7. Characteristic Properties: NIR data
static uint8_t VitaProfileNirDataProps = GATT_PROP_READ | GATT_PROP_NOTIFY;

// 7. Characteristic Description: NIR data
static uint8 VitaProfileNirDataUserDesp[10] = "NIR Data";

// 7. Characteristic Configuration
static gattCharCfg_t *VitaProfileNirDataConfig;

// 8. Characteristic Properties: NIR configuration
static uint8_t VitaProfileNirConfProps = GATT_PROP_READ | GATT_PROP_WRITE;

// 8. Characteristic Value: NIR configuration
static uint8_t VitaProfileNirConf[VITA_NIR_CONF_LEN] = {0};

// 8. Characteristic User Description: NIR configuration
static uint8_t VitaProfileNirConfUserDesp[10] = "NIR Conf";
#endif
/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t VitaProfileAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] =
{
  // Vita Profile Service
  {
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8 *)&VitaProfileService              /* pValue */
  },
#ifdef VITA_BIA_SCAN
    // Characteristic 1 Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &VitaProfileDataProps
    },

      // Characteristic 1 Value
      {
        { ATT_BT_UUID_SIZE, VitaProfileDataUUID },
        GATT_PERMIT_READ,
        0,
        (uint8_t *)&VitaProfileData
      },

      // Characteristic 1 User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        VitaProfileDataUserDesp
      },

      // Characteristic 1 Configuration
      {
        { ATT_BT_UUID_SIZE, clientCharCfgUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8_t *)&VitaProfileDataConfig
      },

    // Characteristic 2 Declaration
    {
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ,
      0,
      &VitaProfileConfProps
    },

      // Characteristic 2 Value
      {
        { ATT_BT_UUID_SIZE, VitaProfileConfUUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0,
        (uint8_t *)&VitaProfileConf
      },

      // Characteristic 2 User Description
      {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        VitaProfileConfUserDesp
      },
#endif
#ifdef BLE_DEBUG
      // Characteristic 3 Declaration
      {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &VitaProfileDebugProps
      },

        // Characteristic 3 Value
        {
          { ATT_BT_UUID_SIZE, VitaProfileDebugUUID },
          GATT_PERMIT_READ,
          0,
          (uint8_t *)&VitaProfileDebug
        },

        // Characteristic 3 User Description
        {
          { ATT_BT_UUID_SIZE, charUserDescUUID },
          GATT_PERMIT_READ,
          0,
          VitaProfileDebugUserDesp
        },

        // Characteristic 3 Configuration
        {
          { ATT_BT_UUID_SIZE, clientCharCfgUUID },
          GATT_PERMIT_READ | GATT_PERMIT_WRITE,
          0,
          (uint8_t *)&VitaProfileDebugConfig
        },

      // Characteristic 4 Declaration
      {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &VitaProfileDebugConfProps
      },

        // Characteristic 4 Value
        {
          { ATT_BT_UUID_SIZE, VitaProfileDebugConfUUID },
          GATT_PERMIT_READ | GATT_PERMIT_WRITE,
          0,
          (uint8_t *)&VitaProfileDebugConf
        },

        // Characteristic 4 User Description
        {
          { ATT_BT_UUID_SIZE, charUserDescUUID },
          GATT_PERMIT_READ,
          0,
          VitaProfileDebugConfUserDesp
        },
#endif
        // Characteristic 5 Declaration
        {
          { ATT_BT_UUID_SIZE, characterUUID },
          GATT_PERMIT_READ,
          0,
          &VitaProfileSpiRespProps
        },

          // Characteristic 5 Value
          {
            { ATT_BT_UUID_SIZE, VitaProfileSpiRespUUID },
            GATT_PERMIT_READ,
            0,
            (uint8_t *)&VitaProfileSpiResp
          },

          // Characteristic 5 User Description
          {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            VitaProfileSpiRespUserDesp
          },

          // Characteristic 5 Configuration
          {
            { ATT_BT_UUID_SIZE, clientCharCfgUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            (uint8_t *)&VitaProfileSpiRespConfig
          },

        // Characteristic 6 Declaration
        {
          { ATT_BT_UUID_SIZE, characterUUID },
          GATT_PERMIT_READ,
          0,
          &VitaProfileSpiReqProps
        },

          // Characteristic 6 Value
          {
            { ATT_BT_UUID_SIZE, VitaProfileSpiReqUUID },
            GATT_PERMIT_READ | GATT_PERMIT_WRITE,
            0,
            (uint8_t *)&VitaProfileSpiReq
          },

          // Characteristic 6 User Description
          {
            { ATT_BT_UUID_SIZE, charUserDescUUID },
            GATT_PERMIT_READ,
            0,
            VitaProfileSpiReqUserDesp
          },
#ifdef VITA_NIR_SCAN
          // Characteristic 7 Declaration
          {
            { ATT_BT_UUID_SIZE, characterUUID },
            GATT_PERMIT_READ,
            0,
            &VitaProfileNirDataProps
          },

            // Characteristic 7 Value
            {
              { ATT_BT_UUID_SIZE, VitaProfileNirDataUUID },
              GATT_PERMIT_READ,
              0,
              (uint8_t *)&VitaProfileNirData
            },

            // Characteristic 7 User Description
            {
              { ATT_BT_UUID_SIZE, charUserDescUUID },
              GATT_PERMIT_READ,
              0,
              VitaProfileNirDataUserDesp
            },

            // Characteristic 7 Configuration
            {
              { ATT_BT_UUID_SIZE, clientCharCfgUUID },
              GATT_PERMIT_READ | GATT_PERMIT_WRITE,
              0,
              (uint8_t *)&VitaProfileNirDataConfig
            },

          // Characteristic 8 Declaration
          {
            { ATT_BT_UUID_SIZE, characterUUID },
            GATT_PERMIT_READ,
            0,
            &VitaProfileNirConfProps
          },

            // Characteristic 8 Value
            {
              { ATT_BT_UUID_SIZE, VitaProfileNirConfUUID },
              GATT_PERMIT_READ | GATT_PERMIT_WRITE,
              0,
              (uint8_t *)&VitaProfileNirConf
            },

            // Characteristic 8 User Description
            {
              { ATT_BT_UUID_SIZE, charUserDescUUID },
              GATT_PERMIT_READ,
              0,
              VitaProfileNirConfUserDesp
            },
#endif
};

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static bStatus_t VitaProfile_ReadAttrCB(uint16_t connHandle,
                                          gattAttribute_t *pAttr,
                                          uint8_t *pValue, uint16_t *pLen,
                                          uint16_t offset, uint16_t maxLen,
                                          uint8_t method);
static bStatus_t VitaProfile_WriteAttrCB(uint16_t connHandle,
                                           gattAttribute_t *pAttr,
                                           uint8_t *pValue, uint16_t len,
                                           uint16_t offset, uint8_t method);

/*********************************************************************
 * PROFILE CALLBACKS
 */

// Vita Profile Service Callbacks
// Note: When an operation on a characteristic requires authorization and
// pfnAuthorizeAttrCB is not defined for that characteristic's service, the
// Stack will report a status of ATT_ERR_UNLIKELY to the client.  When an
// operation on a characteristic requires authorization the Stack will call
// pfnAuthorizeAttrCB to check a client's authorization prior to calling
// pfnReadAttrCB or pfnWriteAttrCB, so no checks for authorization need to be
// made within these functions.
CONST gattServiceCBs_t VITAPROFILECBs =
{
  VitaProfile_ReadAttrCB,  // Read callback function pointer
  VitaProfile_WriteAttrCB, // Write callback function pointer
  NULL                       // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      VitaProfile_AddService
 *
 * @brief   Initializes the Vita Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t VitaProfile_AddService( uint32 services )
{
  uint8 status;
#ifdef VITA_BIA_SCAN
  // Allocate Client Characteristic Configuration table
  VitaProfileDataConfig = (gattCharCfg_t *)ICall_malloc( sizeof(gattCharCfg_t) *
                                                            linkDBNumConns );
  if ( VitaProfileDataConfig == NULL )
  {
    return ( bleMemAllocError );
  }
#endif
#ifdef BLE_DEBUG
  VitaProfileDebugConfig = (gattCharCfg_t *)ICall_malloc( sizeof(gattCharCfg_t) *
                                                            linkDBNumConns );
  if ( VitaProfileDebugConfig == NULL )
  {
    return ( bleMemAllocError );
  }
#endif

  VitaProfileSpiRespConfig = (gattCharCfg_t *)ICall_malloc( sizeof(gattCharCfg_t) *
                                                            linkDBNumConns );
  if ( VitaProfileSpiRespConfig == NULL )
  {
    return ( bleMemAllocError );
  }
#ifdef VITA_NIR_SCAN
  VitaProfileNirDataConfig = (gattCharCfg_t *)ICall_malloc( sizeof(gattCharCfg_t) *
                                                            linkDBNumConns );
  if ( VitaProfileNirDataConfig == NULL )
  {
    return ( bleMemAllocError );
  }
#endif
  // Initialize Client Characteristic Configuration attributes
#ifdef VITA_BIA_SCAN
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, VitaProfileDataConfig );
#endif
#ifdef BLE_DEBUG
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, VitaProfileDebugConfig );
#endif
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, VitaProfileSpiRespConfig );
#ifdef VITA_NIR_SCAN
  GATTServApp_InitCharCfg( INVALID_CONNHANDLE, VitaProfileNirDataConfig );
#endif
  if ( services & VITAPROFILE_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( VitaProfileAttrTbl,
                                          GATT_NUM_ATTRS( VitaProfileAttrTbl ),
                                          GATT_MAX_ENCRYPT_KEY_SIZE,
                                          &VITAPROFILECBs );
  }
  else
  {
    status = SUCCESS;
  }

  return ( status );
}

/*********************************************************************
 * @fn      VitaProfile_RegisterAppCBs
 *
 * @brief   Registers the application callback function. Only call
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t VitaProfile_RegisterAppCBs( VitaProfileCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
      VitaProfile_AppCBs = appCallbacks;

    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}

/*********************************************************************
 * @fn      VitaProfile_SetParameter
 *
 * @brief   Set a Vita Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to write
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t VitaProfile_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
#ifdef VITA_BIA_SCAN
    case VITAPROFILE_DATA:
      if ( len == VITA_DATA_LEN)
      {
          memcpy(VitaProfileData, value, VITA_DATA_LEN);

          // See if Notification has been enabled
          GATTServApp_ProcessCharCfg( VitaProfileDataConfig, &VitaProfileData, FALSE,
                                      VitaProfileAttrTbl, GATT_NUM_ATTRS( VitaProfileAttrTbl ),
                                      INVALID_TASK_ID, VitaProfile_ReadAttrCB );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case VITAPROFILE_CONF:
      if ( len == VITA_CONF_LEN)
      {
          memcpy(VitaProfileConf, value, VITA_CONF_LEN);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
#endif
#ifdef BLE_DEBUG
    case VITAPROFILE_DEBUG:
      if ( len == VITA_DEBUG_LEN )
      {
          memcpy(VitaProfileDebug, value, VITA_DEBUG_LEN);
          // See if Notification has been enabled
          GATTServApp_ProcessCharCfg( VitaProfileDebugConfig, &VitaProfileDebug, FALSE,
                                      VitaProfileAttrTbl, GATT_NUM_ATTRS( VitaProfileAttrTbl ),
                                      INVALID_TASK_ID, VitaProfile_ReadAttrCB );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case VITAPROFILE_DEBUG_CONF:
      if ( len == VITA_DEBUG_CONF_LEN)
      {
          memcpy(VitaProfileDebugConf, value, VITA_DEBUG_CONF_LEN);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
#endif
    case VITAPROFILE_SPI_RESP:
      if ( len == VITA_SPI_RESP_LEN )
      {
          memcpy(VitaProfileSpiResp, value, VITA_SPI_RESP_LEN);

          // See if Notification has been enabled
          GATTServApp_ProcessCharCfg( VitaProfileSpiRespConfig, &VitaProfileSpiResp, FALSE,
                                      VitaProfileAttrTbl, GATT_NUM_ATTRS( VitaProfileAttrTbl ),
                                      INVALID_TASK_ID, VitaProfile_ReadAttrCB );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case VITAPROFILE_SPI_REQ:
      //if ( len == VITA_SPI_REQ_LEN)
      {
          memcpy(VitaProfileSpiReq, value, VITA_SPI_REQ_LEN);
      }
      //else
      //{
      //  ret = bleInvalidRange;
      //}
      break;
#ifdef VITA_NIR_SCAN
    case VITAPROFILE_NIR_DATA:
      if ( len == VITA_NIR_DATA_LEN)
      {
          memcpy(VitaProfileNirData, value, VITA_NIR_DATA_LEN);

          // See if Notification has been enabled
          GATTServApp_ProcessCharCfg( VitaProfileNirDataConfig, &VitaProfileNirData, FALSE,
                                      VitaProfileAttrTbl, GATT_NUM_ATTRS( VitaProfileAttrTbl ),
                                      INVALID_TASK_ID, VitaProfile_ReadAttrCB );
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case VITAPROFILE_NIR_CONF:
      if ( len == VITA_NIR_CONF_LEN)
      {
          memcpy(VitaProfileNirConf, value, VITA_NIR_CONF_LEN);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;
#endif
    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

/*********************************************************************
 * @fn      VitaProfile_GetParameter
 *
 * @brief   Get a Vita Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t VitaProfile_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
#ifdef VITA_BIA_SCAN
    case VITAPROFILE_DATA:
        VOID memcpy( value, VitaProfileData, VITA_DATA_LEN );
      break;

    case VITAPROFILE_CONF:
        VOID memcpy( value, VitaProfileConf, VITA_CONF_LEN );
      break;
#endif
#ifdef BLE_DEBUG
    case VITAPROFILE_DEBUG:
        VOID memcpy( value, VitaProfileDebug, VITA_DEBUG_LEN );
      break;

    case VITAPROFILE_DEBUG_CONF:
        VOID memcpy( value, VitaProfileDebugConf, VITA_DEBUG_CONF_LEN );
      break;
#endif
    case VITAPROFILE_SPI_RESP:
        VOID memcpy( value, VitaProfileSpiResp, VITA_SPI_RESP_LEN );
      break;

    case VITAPROFILE_SPI_REQ:
        VOID memcpy( value, VitaProfileSpiReq, VITA_SPI_REQ_LEN );
      break;
#ifdef VITA_NIR_SCAN
    case VITAPROFILE_NIR_DATA:
        VOID memcpy( value, VitaProfileNirData, VITA_NIR_DATA_LEN );
      break;

    case VITAPROFILE_NIR_CONF:
        VOID memcpy( value, VitaProfileNirConf, VITA_NIR_CONF_LEN );
      break;
#endif
    default:
      ret = INVALIDPARAMETER;
      break;
  }

  return ( ret );
}

/*********************************************************************
 * @fn          VitaProfile_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 * @param       method - type of read message
 *
 * @return      SUCCESS, blePending or Failure
 */
static bStatus_t VitaProfile_ReadAttrCB(uint16_t connHandle,
                                          gattAttribute_t *pAttr,
                                          uint8_t *pValue, uint16_t *pLen,
                                          uint16_t offset, uint16_t maxLen,
                                          uint8_t method)
{
  bStatus_t status = SUCCESS;

  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those reads
#ifdef VITA_BIA_SCAN
      case VITA_DATA_UUID:
        *pLen = VITA_DATA_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_DATA_LEN );
        break;

      case VITA_CONF_UUID:
        *pLen = VITA_CONF_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_CONF_LEN );
        break;
#endif
#ifdef BLE_DEBUG
      case VITA_DEBUG_UUID:
        *pLen = VITA_DEBUG_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_DEBUG_LEN );
        break;

      case VITA_DEBUG_CONF_UUID:
        *pLen = VITA_DEBUG_CONF_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_DEBUG_CONF_LEN );
        break;
#endif
      case VITA_SPI_RESP_UUID:
        *pLen = VITA_SPI_RESP_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_SPI_RESP_LEN );
        break;

      case VITA_SPI_REQ_UUID:
        *pLen = VITA_SPI_REQ_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_SPI_REQ_LEN );
        break;
#ifdef VITA_NIR_SCAN
      case VITA_NIR_DATA_UUID:
        *pLen = VITA_NIR_DATA_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_NIR_DATA_LEN );
        break;

      case VITA_NIR_CONF_UUID:
        *pLen = VITA_NIR_CONF_LEN;
        VOID memcpy( pValue, pAttr->pValue, VITA_NIR_CONF_LEN );
        break;
#endif
      default:
        // Should never get here! (characteristics 3 and 4 do not have read permissions)
        *pLen = 0;
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    *pLen = 0;
    status = ATT_ERR_INVALID_HANDLE;
  }

  return ( status );
}

/*********************************************************************
 * @fn      VitaProfile_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t VitaProfile_WriteAttrCB(uint16_t connHandle,
                                           gattAttribute_t *pAttr,
                                           uint8_t *pValue, uint16_t len,
                                           uint16_t offset, uint8_t method)
{
  bStatus_t status = SUCCESS;
  uint8 notifyApp = 0xFF;

  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
#ifdef VITA_BIA_SCAN
      case VITA_CONF_UUID:
          //Validate the value
          // Make sure it's not a blob oper
          if ( offset == 0 )
          {
            if ( len != VITA_CONF_LEN )
            {
              status = ATT_ERR_INVALID_VALUE_SIZE;
            }
          }
          else
          {
            status = ATT_ERR_ATTR_NOT_LONG;
          }

          //Write the value
          if ( status == SUCCESS )
          {
            memcpy(pAttr->pValue, pValue, VITA_CONF_LEN);

            if( pAttr->pValue == (uint8_t*)&VitaProfileConf )
            {
              notifyApp = VITAPROFILE_CONF;
            }
          }
          break;
#endif
#ifdef BLE_DEBUG
      case VITA_DEBUG_CONF_UUID:

          //Validate the value
          // Make sure it's not a blob oper
          if ( offset == 0 )
          {
            if ( len != VITA_DEBUG_CONF_LEN )
            {
              status = ATT_ERR_INVALID_VALUE_SIZE;
            }
          }
          else
          {
            status = ATT_ERR_ATTR_NOT_LONG;
          }

          //Write the value
          if ( status == SUCCESS )
          {
            memcpy(pAttr->pValue, pValue, VITA_DEBUG_CONF_LEN);

            if( pAttr->pValue == (uint8_t*)&VitaProfileDebugConf)
            {
              notifyApp = VITAPROFILE_DEBUG_CONF;
            }
          }
          break;
#endif
      case VITA_SPI_REQ_UUID:
          //Validate the value
          // Make sure it's not a blob oper
          if ( offset == 0 )
          {
            if ( len != VITA_SPI_REQ_LEN )
            {
              status = ATT_ERR_INVALID_VALUE_SIZE;
            }
          }
          else
          {
            status = ATT_ERR_ATTR_NOT_LONG;
          }

          //Write the value
          if ( status == SUCCESS )
          {
              memcpy(pAttr->pValue, pValue, VITA_SPI_REQ_LEN);

            if( pAttr->pValue == (uint8_t*)&VitaProfileSpiReq )
            {
              notifyApp = VITAPROFILE_SPI_REQ;
            }
          }
          break;
#ifdef VITA_NIR_SCAN
      case VITA_NIR_CONF_UUID:
          //Validate the value
          // Make sure it's not a blob oper
          if ( offset == 0 )
          {
            if ( len != VITA_NIR_CONF_LEN )
            {
              status = ATT_ERR_INVALID_VALUE_SIZE;
            }
          }
          else
          {
            status = ATT_ERR_ATTR_NOT_LONG;
          }

          //Write the value
          if ( status == SUCCESS )
          {
              memcpy(pAttr->pValue, pValue, VITA_NIR_CONF_LEN);

            if( pAttr->pValue == (uint8_t*)&VitaProfileNirConf )
            {
              notifyApp = VITAPROFILE_NIR_CONF;
            }
          }
          break;
#endif

      case GATT_CLIENT_CHAR_CFG_UUID:
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        break;

      default:
        // Should never get here!
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    status = ATT_ERR_INVALID_HANDLE;
  }

  // If a characteristic value changed then callback function to notify application of change
  if ( (notifyApp != 0xFF ) && VitaProfile_AppCBs && VitaProfile_AppCBs->pfnVitaProfileChange )
  {
      VitaProfile_AppCBs->pfnVitaProfileChange( notifyApp );
  }

  return ( status );
}

/*********************************************************************
*********************************************************************/
